import { createApp } from 'vue'
import App from './App.vue'
import router from './router'
import store from './store'

// Buefy and bulma
import Oruga from '@oruga-ui/oruga-next'
import { bulmaConfig } from '@oruga-ui/theme-bulma'
import '@oruga-ui/theme-bulma/dist/bulma.css'

// font awesome
import { library } from '@fortawesome/fontawesome-svg-core'
import { fas } from '@fortawesome/free-solid-svg-icons'
import { FontAwesomeIcon } from '@fortawesome/vue-fontawesome'

library.add(fas)

const app = createApp(App);

app.component('vue-fontawesome', FontAwesomeIcon)

app.use(Oruga, bulmaConfig);
app.use(store);
app.use(router);

app.config.productionTip = true

app.mount('#app')
