import { createStore } from 'vuex'

var endCampaign = 1614513438000
export default new createStore({
  state: {
    count: 3,
    config: {
      crepe: {
        enabled: true,
        minDate: new Date().getTime(),
        maxDate: endCampaign,
        livraison: true,
        livraisonAix: true,
        choices: {
          nutella: {
            name: 'Nutella',
            cost: 0,
            count: 0
          },

          sucre_moins: {
            name: 'Un peu moins de sucre',
            cost: 0,
            count: 0
          },
          sucre_plus: {
            name: 'Un peu plus de sucre',
            cost: 0,
            count: 0
          },
          citron_sucre: {
            name: 'Citron sucre',
            cost: 0,
            count: 0
          },
          confiture_fraise: {
            name: 'Confiture de fraise',
            cost: 0,
            count: 0
          },
          confiture_myrtille: {
            name: 'Confiture de myrtille',
            cost: 0,
            count: 0
          },
          miel: {
            name: 'Miel',
            cost: 0,
            count: 0
          },
          caramel: {
            name: 'Caramel',
            cost: 0,
            count: 0
          },

          creme_de_marron: {
            name: 'Creme de marron',
            cost: 0,
            count: 0
          },
          nature: {
            name: 'Nature',
            cost: 0,
            count: 0
          }
        }
      },
      dej: {
        enabled: false,
        minDate: new Date().getTime(),
        maxDate: endCampaign,
        boissons: {
          the: {
            name: 'Thé',
            cost: 0,
            count: 0
          },
          cafe: {
            name: 'Cafe',
            cost: 0,
            count: 0
          },
          jus_orange: {
            name: "Jus d'orange",
            cost: 0,
            count: 0
          },
          lait_chocolat_chaud: {
            name: 'Chocolat au lait chaud',
            cost: 0,
            count: 0
          },
          lait_chocolat_froid: {
            name: 'Chocolat au lait froid',
            cost: 0,
            count: 0
          }
        },
        fr: {
          name: 'Petit déjeuner français',
          choices: {
            toast: {
              name: 'Toast',
              count: true
            },

            gaufre_nature: {
              name: 'Gaufre nature',
              unique: true,
              count: true
            },
            gaufre_nutella: {
              name: 'Gaufre nutella',
              count: 0,
              unique: 0
            },
            gaufre_sucre: {
              name: 'Gaufre sucre',
              unique: true,
              count: 0
            },
            gaufre_confiture_fraise: {
              name: 'Gaufre confiture fraise',
              unique: true,
              count: 0
            },
            compote: {
              name: 'Compote',
              count: 0
            }
          }
        },
        en: {
          name: 'Petit déjeuner anglais',
          choices: {
            bacon: {
              name: 'Bacon',
              cost: 0,
              count: 0
            },

            pancakes_nutella: {
              name: 'Pancakes nutella',
              count: 0,
              unique: true
            },
            pancakes_de_marron: {
              name: 'Pancakes crême de marron',
              count: 0,
              unique: true
            },
            pancakes_caramel: {
              name: 'Pancakes caramel',
              count: 0,
              unique: true
            },
            oeuf_brouille: {
              name: 'Oeuf brouillé',
              count: true
            }
          }
        }
      },
      kebab: {
        enabled: false,
        livraison: false,
        livraisonAix: false,
        later: true,
        livraisonText: '',
        minDate: 1613728800000,
        maxDate: 1613739600000,
        name: 'Operation kebab',
        choices: {
          keb_viande: { name: 'Filet de dinde unique', count: true },
          keb_salade: { name: 'Salade', count: 0 },
          keb_tomate: { name: 'Tomate', count: 0 },
          keb_oignons: { name: 'Oignons rouges', count: 0 },
          keb_concompbre: { name: 'Concombre', count: 0 },
          keb_choux_rouges: { name: 'Choux rouges', count: 0 },
          keb_frites: { name: 'Frites classiques', count: true, unique: true },
          keb_patates: { name: 'Patates douces', count: 0, unique: true }
        },
        boissons: {
          icetea: {
            name: 'Ice tea'
          },
          coca: {
            name: 'Coca Cola'
          },
          lipton: {
            name: 'Lipton'
          },
          none: {
            name: 'Aucune'
          }
        },
        sauce: {
          sauce_barbeq: {
            name: 'Barbeq'
          },
          sauce_ketchup: {
            name: 'Ketchup'
          },
          sauce_mayo: {
            name: 'Mayonnnaise'
          },
          sauce_blanche: {
            name: 'Blanche'
          },
          sauce_samourai: {
            name: 'Samouraï'
          },
          sauce_algerienne: {
            name: 'Algérienne'
          },
          sauce_turque: {
            name: 'Turque'
          },
          sauce_marocaine: {
            name: 'Marocaine'
          },
          sauce_tunisienne: {
            name: 'Tunisienne'
          },
          sauce_andalouse: {
            name: 'Andalouse'
          },
          sauce_biggy: {
            name: 'Biggy'
          },
          none: {
            name: 'Aucune'
          }
        }
      },
      lasagne: {
        enabled: false,
        choices: {
          lasagne: {
            name: 'Lasagne',
            cost: 2,
            count: 0
          },
          lasagne_vege: {
            name: 'Lasagne vegetarienne',
            cost: 2,
            count: 0
          }
        }
      }
    }
  },
  mutations: {
    setConfig(state, payload) {
      state.config = payload
    }
  },
  actions: {
    async loadData({ commit }) {
      fetch('https://minesterydemo.henri2h.fr/api/config', {
        method: 'GET',
        headers: {
          Accept: 'application/json',
          'Content-Type': 'application/json'
        }
      })
        .then(response => {
          return response.json()
        })
        .then(result => {
          console.log(result)
          if (result.success) {
            commit('setConfig', result.result)
          }
          else {
            console.log(result.err)
          }
        })
        .catch(err => {
          console.error(err)
          this.loading = false
        })
    }
  },
  modules: {}
})
