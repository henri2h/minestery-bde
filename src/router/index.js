import { createRouter, createWebHashHistory } from 'vue-router'

import Home from '../views/HomePage.vue'
import Order from '../views/OrderPage.vue'
import Success from '../views/SuccessPage.vue'
import Orders from '../views/OrdersPage.vue'
import Config from '../views/ConfigPage.vue'
import Comment from '../views/CommentPage.vue'
import MinesDeRien from '../views/MinesDeRienPage.vue'
import Stats from '../views/StatsPage.vue'

const routes = [
  {
    path: '/',
    name: 'Home',
    component: Home
  },
  {
    path: '/order',
    name: 'Order',
    component: Order
  },
  {
    path: '/order/:cmode',
    name: 'Commande ',
    component: Order,
    props: true
  },

  {
    path: '/order_type/minesverick',
    name: 'Commande Minverick',
    component: Order,
    props: { cmode: 'crepe', listeIn: 'minesverick' }
  },
  {
    path: '/order_type/rand',
    name: 'Commande random',
    component: Order,
    props: { cmode: 'crepe', listeIn: 'rand' }
  },
  {
    path: '/order_type/astronomines',
    name: 'Commande Astronomines',
    component: Order,
    props: { cmode: 'crepe', listeIn: 'astronomines' }
  },
  {
    path: '/success',
    name: 'Success',
    component: Success
  },
  {
    path: '/comment',
    name: 'Comment',
    component: Comment
  },
  {
    path: '/orders/:token',
    name: 'Commandes',
    component: Orders,
    props: true
  },
  {
    path: '/config/:token',
    name: 'Config',
    component: Config,
    props: true
  },
  {
    path: '/minesderien',
    name: "C'est un peu vide ici",
    component: MinesDeRien
  },
  {
    path: '/stats',
    name: 'Oh des stats',
    component: Stats
  }
]

const router = new createRouter({
  history: createWebHashHistory(),
  routes
})

export default router
